# Mage2 Module Kowal CategoryCustomDescription

    ``kowal/module-categorycustomdescription``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Dodatkowe pola opisowe kategorii

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_CategoryCustomDescription`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-categorycustomdescription`
 - enable the module by running `php bin/magento module:enable Kowal_CategoryCustomDescription`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration




## Specifications

 - Widget
	- customDescription


## Attributes

 - Category - Custom Description 1 (custom_description_1)

 - Category - Custom Description 2 (custom_description_2)

 - Category - Custom Description 3 (custom_description_3)

 - Category - Custom Description 4 (custom_description_4)

