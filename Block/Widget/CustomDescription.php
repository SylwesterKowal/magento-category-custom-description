<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\CategoryCustomDescription\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class CustomDescription extends Template implements BlockInterface
{

    protected $_template = "widget/customdescription.phtml";

}

